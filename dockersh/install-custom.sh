#!/bin/sh
# Original by https://github.com/sleeepyjack/dockersh
# Modified by georg@lysergic.dev

pip3 install --upgrade -r requirements.txt
if [ -z "$1" ]; then
    activate-global-python-argcomplete
else
    activate-global-python-argcomplete --dest=$1
fi
cp dockersh /opt/dockersh/bin/
chmod +x /opt/dockersh/bin/dockersh
cp -n dockersh.ini /opt/dockersh/etc/
