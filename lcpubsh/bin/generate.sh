#!/bin/sh
# georg@lysergic.dev
set -e
OUTPUT="nc -N 127.0.0.2 2424"
echo "Shell generation invoked." | $OUTPUT
if [ ! "$#" -eq 0 ]; then
user="$(echo "$1" |tr '[:upper:]' '[:lower:]')"
case "$2" in
        "archlinux")
                os="Arch Linux"
                image="lc-archlinux-userbase-v2:sh0"
                ;;
        "ubuntu")
                os="Ubuntu (Latest)"
                image="lcbase_ubuntu_14082021_2:sh0"
                ;;
        *)
                echo "Choose between archlinux or ubuntu"
                exit 1
                ;;
esac
fingerprint_ecdsa="$(ssh-keygen -lf /etc/ssh/ssh_host_ecdsa_key.pub)"
if id "$user" &>/dev/null; then
        echo "Aborted. Username is already taken."
        echo "Aborted: $user is already taken." | $OUTPUT
else
        echo "Hang on ..."
        echo "Creating $user locally." | $OUTPUT
        sudo useradd -mUs /opt/lcpubsh/bin/pubsh -G docker $user
        pass=$(shuf -n2 /usr/share/dict/words | tr -d '\n')
        echo "Appending to config." | $OUTPUT
        echo "" >> /etc/dockersh.ini
        echo "[$user]" >> /etc/dockersh.ini
        echo "image = $user" >> /etc/dockersh.ini
        echo "Forking Docker base image ($image)." | $OUTPUT
        /opt/lcpubsh/bin/make_lc_user_image.sh $user $image | $OUTPUT
        echo "Setting password." | $OUTPUT
        sudo /opt/adduser.sh $user $pass
        echo "@$user A shell with the operating system $os has been spawned for you." | $OUTPUT
        echo "@$user Option A) Connect directly:" | $OUTPUT
        echo "@$user ssh -p 2222 $user@sh.lib.casa" | $OUTPUT
        echo "@$user Option B) Connect through Tor:" | $OUTPUT
        echo "@$user torsocks ssh -p 2222 $user@yib4545ooc4bndo27tcdd4jdeqsfnjm44yacusemwf5eu7lnlwskt5ad.onion" | $OUTPUT
        echo "@$user Confirm the fingerprint (displayed upon connecting for the first time) matches the one shown here:" | $OUTPUT
        echo "@$user $fingerprint_ecdsa" | $OUTPUT
        echo "@$user And finally, here's your password:" | $OUTPUT
        echo "@$user $pass" | $OUTPUT
        echo "@$user Have fun!" | $OUTPUT
        echo "#universe $pass" | $OUTPUT
        echo "Done." | $OUTPUT
        echo "OK. Details sent to user and/or admins."
fi
else
        echo "No argument supplied."
fi
