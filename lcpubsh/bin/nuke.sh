#!/bin/sh
# georg@lysergic.dev
set -e
OUTPUT="nc -N 127.0.0.2 2424"
echo "Shell removal invoked." | $OUTPUT
if [ ! "$#" -eq 0 ]; then
user="$(echo "$1" |tr '[:upper:]' '[:lower:]')"
if id "$user" &>/dev/null; then
        CONTAINER="$(docker ps -qf "name=$user")"
        echo "User: $user - Container ID: $CONTAINER" | $OUTPUT
        if [ ! -z "$CONTAINER" ]; then
        docker stop $CONTAINER
        if [ $? -eq "0" ]; then
        echo "Running shell found and stopped."
        else
        echo "Shell could not be stopped. Maybe it's lready down?"
        fi
        docker rm $CONTAINER
        if [ $? -eq "0" ]; then
        echo "Shell container found and removed."
        else
        echo "Shell could not be removed. Maybe it doesn't exist?"
        fi
        else
        echo "ID could not be determined."
        fi
        sudo /usr/sbin/userdel -f $user | $OUTPUT
        sudo /usr/sbin/groupdel -f $user | $OUTPUT
        sudo /usr/bin/rm -rf /home/$user
        sudo /usr/bin/rm -rf /var/spool/mail/$user
        INITEMP=$(mktemp --tmpdir initemp.XXXXX)
        trap "rm -f $INITEMP" 0 1 2 3 15
        sed "/$user/d" "/etc/dockersh.ini" > "$INITEMP"
        cat "$INITEMP" > /etc/dockersh.ini
        echo "Done." | $OUTPUT
        echo "OK."
else
        echo "Aborted: User not found."
fi
else
        echo "No argument supplied."
fi
