#!/bin/bash
# Original by https://github.com/sleeepyjack/dockersh
# Modified by georg@lysergic.dev

if [ -z "$1" -o -z "$2" ]; then
        echo "./make_user_image.sh [name] [source-image]"; exit 100
fi

sed -i "1s/.*/FROM $2/" /opt/dockersh/dockersh-git/image_template/Dockerfile
cd /opt/dockersh/dockersh-git/image_template
docker build -t $1:sh0 .

